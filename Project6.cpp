﻿#include <iostream>
#include <string>

class MyClass
{
public:
	void SetName(std::string newName)
	{
		Name = newName;
	}
	void SetPoints(int newPoints)
	{
		Points = newPoints;
	}
private:
	int Points = 0;
	std::string Name = "WhoAreYou";
};


int main()
{
	int Num;
	std::cout << "How many players there will be?";
	std::cin >> Num;
// По идее нужно создать массив, который будет хранить в себе объекты класса
// Но какой тогда это тип массива? int?
// Пришлось сделать 2 массива и сортировать оба в зависимости от Points
	int* arrive = new int[Num];
	std::string* nam = new std::string[Num];
	for (int i = 0; i < Num; i++)
	{
		std::cout << "Player:";
		std::cin >> nam[i];
		std::cout << "Points:";
		std::cin >> arrive[i];//Добавить в два массива по очереди значения
		
	}
	for (int i = 0; i < (Num-1); i++)
	{
		for (int j = 0; j < (Num-1); j++)
		{
			if (arrive[j] > arrive[j + 1])
			{
				int perem= arrive[j];
				arrive[j] = arrive[j+1];
				arrive[j+1] = perem;
				std::string znach = nam[j];
				nam[j] = nam[j + 1];
				nam[j+1] = znach;
			}
		}
	}

	for (int i = 0; i < Num; i++)
	{
		std::cout << "Player: " << nam[i] << " have " << arrive[i] << " points\n";
	}

	delete [] arrive;
	delete [] nam;
}
